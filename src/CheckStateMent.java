public class CheckStateMent {
    private int income;
    private int monthlyPay;
    private String name;

    public CheckStateMent(String name, int income, int monthlyPay) {
        this.name = name;
        this.income = income;
        this.monthlyPay = monthlyPay;
    }

    public int getIncome() {
        return income;
    }

    public int getmonthlyPay() {
        return monthlyPay;
    }

    public String getName() {
        return name;
    }

    public void print() {
        if (income > monthlyPay * 3) {
            System.out.println(name + " Your Statement is Passed!!!");
        } else {
            System.out.println(name + " Your Statement is Not Passed!!!");
        }
    }
}
