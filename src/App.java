
public class App {
    public static void main(String[] args) {
        Pay somChai = new Pay("SomChai", "Bike", 100000, 15, 4, 3.95);
        Pay somPong = new Pay("SomPong", "Luxury Car", 1280000, 30, 6, 8);
        Pay somSrieee = new Pay("SomSrieee", "Car", 500000, 10, 6, 3.5);
        somChai.print();
        somPong.print();
        somSrieee.print();
        CheckStateMent somChai1 = new CheckStateMent("SomChai", 30000, 2850);
        CheckStateMent somPong1 = new CheckStateMent("SomPong", 50000, 18750);
        CheckStateMent somSrineee1 = new CheckStateMent("SomSrieee", 21000, 7850);
        somChai1.print();
        somPong1.print();
        somSrineee1.print();
    }
}