
public class Pay {
    private int fullPrice;
    private int downPayment;
    private int year;
    private double increase;
    private String name;
    private String carType;

    public Pay(String name, String carType, int fullPrice, int downPayment, int year, double increase) {
        this.name = name;
        this.fullPrice = fullPrice;
        this.downPayment = downPayment;
        this.year = year;
        this.increase = increase;
        this.carType = carType;
    }

    public String getName() {
        return name;
    }

    public String getCarType() {
        return carType;
    }

    public int fullPrice() {
        return fullPrice;
    }

    public int downPayment() {
        return downPayment;
    }

    public int year() {
        return year;
    }

    public double increase() {
        return increase;
    }

    private void paymentCalculator() {
        double downPaymentPercent = fullPrice * downPayment / 100;
        double lastPrice = fullPrice - downPaymentPercent;
        double result = (lastPrice + ((lastPrice * increase / 100) * year));
        System.out.println(name + " have to pays " + (result / (year * 12) + " Baht per month"));
    }

    public void print() {
        System.out.println("-------------------------------------------------------");
        System.out.println("Your Vehicle is : " + carType);
        System.out.println("Price is : " + fullPrice);
        System.out.println("Your DownPayment is : " + downPayment);
        System.out.println("Increase is  : " + increase);
        System.out.println(year + " Years");
        paymentCalculator();
        System.out.println("-------------------------------------------------------");
    }
}